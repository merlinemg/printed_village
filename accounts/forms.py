from django import forms
from django.forms import PasswordInput, TextInput,FileInput
from django.contrib.auth import authenticate, login

from accounts.models import Login

class LoginForm(forms.ModelForm):

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if email and password:
            user = None
            try:
                user = Login.objects.get(email=email,
                                           password=password)
            except:
                raise forms.ValidationError("Please enter a correct email and password.")

            return self.cleaned_data

    class Meta:
        model = Login
        fields = ['email', 'password']