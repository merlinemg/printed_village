from django.conf.urls import url
from accounts.views import AdminLoginView, LogoutView

urlpatterns = [
    url(r'^$', AdminLoginView.as_view(), name='adminlogin'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),

]
