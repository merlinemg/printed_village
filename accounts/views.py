from django.shortcuts import render
from django.views.generic import FormView, TemplateView, RedirectView
from django.contrib.auth import authenticate, login, logout
# Create your views here.
from accounts.forms import LoginForm
from accounts.models import Login


# Create your views here.
from art_profile.models import Category


class AdminLoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super(AdminLoginView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        return context

    def form_valid(self, form):
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        user = Login.objects.get(email=email,
                                password=password)
        login(self.request, user)
        return super(AdminLoginView, self).form_valid(form)

    def form_invalid(self, form):
        return super(AdminLoginView, self, ).form_invalid(form)



class LogoutView(RedirectView):
    url = '/'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)
