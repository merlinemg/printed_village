from __future__ import unicode_literals

from django.apps import AppConfig


class ArtProfileConfig(AppConfig):
    name = 'art_profile'
