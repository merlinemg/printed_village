from fileinput import FileInput

from django import forms
from django.forms import PasswordInput, TextInput,FileInput
from django.contrib.auth import authenticate, login

from art_profile.models import Login_info_table

class Art_imageForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(Art_imageForm, self).__init__(*args, **kwargs)
        self.fields['category'].empty_label = 'Select Category'
        self.fields['category'].widget.attrs.update({'onchange': 'categorys(this);'})
        self.fields['title'].widget.attrs.update({'class': 'form-control'})
        self.fields['category'].widget.attrs.update({'class': 'form-control'})

        self.fields['description'].widget.attrs.update({'class': 'form-control'})
        self.fields['image_filename'].widget.attrs.update({'class': 'file-upload-input', 'onchange': 'readURL(this);'})
        self.fields['description'].widget.attrs.update({'class': 'form-control'})


    class Meta:
        model = Login_info_table
        fields = ['category', 'title', 'description', 'title', 'image_filename', 'content_adult_type', 'copyright']
        exclude = ['product_size']

    # def save(self, user, commit=True):
    #     form = super(Art_imageForm, self).save(commit=False)
    #     if commit:
    #         form.user = user
    #         form.save()
    #     return form

class UploaddetailForm(forms.ModelForm):


    class Meta:
        model = Login_info_table
        fields = ['image_filename']

    def save(self, user, commit=True):
        form = super(UploaddetailForm, self).save(commit=False)
        if commit:
            form.user = user
            form.save()
        return form