# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-06-02 12:31
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('categorys', models.CharField(blank=True, max_length=50, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Login_info_table',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=100, null=True)),
                ('image_filename', models.ImageField(upload_to='arts_upload')),
                ('description', models.CharField(blank=True, max_length=250, null=True)),
                ('content_adult_type', models.BooleanField(default=False)),
                ('copyright', models.BooleanField(default=False)),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='art_category', to='art_profile.Category')),
            ],
        ),
        migrations.CreateModel(
            name='SubCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(blank=True, max_length=450, null=True)),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tag_category', to='art_profile.Category')),
            ],
        ),
        migrations.AddField(
            model_name='login_info_table',
            name='tags',
            field=models.ManyToManyField(related_name='art_tags', to='art_profile.SubCategory'),
        ),
        migrations.AddField(
            model_name='login_info_table',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='art_detail', to=settings.AUTH_USER_MODEL),
        ),
    ]
