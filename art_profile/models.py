from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils.crypto import get_random_string
from django.utils.text import slugify
# Create your models here.

class Category(models.Model):
    categorys = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.categorys


class SubCategory(models.Model):
    tag = models.CharField(max_length=450, null=True, blank=True)
    category = models.ForeignKey(Category, related_name='tag_category', null=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.tag
class Maincategory(models.Model):
    main_categorys = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.main_categorys

class Productstyle(models.Model):
    product_styles = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.product_styles

class Productsize(models.Model):
    product_sizes = models.CharField(max_length=250, null=True, blank=True)
    product_style = models.ForeignKey(Productstyle, related_name='productstyle', null=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.product_sizes



class Login_info_table(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='art_detail')
    title = models.CharField(max_length=100, null=True, blank=True)
    image_filename = models.ImageField(upload_to='arts_upload')
    main_category = models.ForeignKey(Maincategory, related_name='main_category', null=True, blank=True)
    category = models.ForeignKey(Category, related_name='art_category', null=True, blank=True)
    product_style = models.ForeignKey(Productstyle, related_name='product_style', null=True, blank=True)
    tags = models.ManyToManyField(SubCategory, related_name='art_tags')
    product_size = models.ForeignKey(Productsize, related_name='product_size', null=True, blank=True)
    like_count = models.IntegerField(default=0)
    dislike_count = models.IntegerField(default=0)
    Price = models.IntegerField(default=1)
    designer = models.CharField(max_length=100, null=True, blank=True)
    description = models.CharField(max_length=250, null=True, blank=True)
    content_adult_type = models.BooleanField(default=False)
    copyright = models.BooleanField(default=False)
    status = models.CharField(default="Draft", null=True,blank=True,max_length=50)
    slug = models.SlugField(max_length=255, unique=True, auto_created=True, editable=False)

    def save(self, *args, **kwargs):
        super(Login_info_table, self).save(*args, **kwargs)
        if not self.slug:
            unique_id = get_random_string(length=15)
            self.slug = slugify(self.title) + "-" + unique_id
            self.save()

    class Meta:
        ordering = ['-like_count']
    def __str__(self):
        return self.user.first_name

class Collection(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='collection_detail', null=True, blank=True)
    collection_name = models.CharField(max_length=200, null=True, blank=True)
    collection_description = models.CharField(max_length=250, null=True, blank=True)
    products = models.ManyToManyField(Login_info_table, related_name='collection_products', null=True, blank=True)
    slug = models.SlugField(max_length=255, unique=True, auto_created=True, editable=False)

    def save(self, *args, **kwargs):
        super(Collection, self).save(*args, **kwargs)
        if not self.slug:
            unique_id = get_random_string(length=15)
            self.slug = slugify(self.collection_name) + "-" + unique_id
            self.save()


    def __str__(self):  # __unicode__ on Python 2
        return self.collection_name
