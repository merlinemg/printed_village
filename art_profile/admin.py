from django.contrib import admin

# Register your models here.
from .models import SubCategory, Login_info_table, Category, Collection, Productsize, Productstyle, Maincategory

# Register your models here.
admin.site.register(Login_info_table)
admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(Collection)
admin.site.register(Productsize)
admin.site.register(Productstyle)
admin.site.register(Maincategory)
