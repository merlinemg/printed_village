from django.conf.urls import url
from art_profile.views import SuccessView, PublishView, AllproductView, SingleproductView

urlpatterns = [
    url(r'^add-detail/', SuccessView.as_view(), name='add'),
    url(r'^publish/', PublishView.as_view(), name='publish'),
    url(r'^all_products/', AllproductView.as_view(), name='all_products'),
    url(r'^(?P<productlist_slug>[\w-]+)/', SingleproductView.as_view(), name='singleproduct'),


]
