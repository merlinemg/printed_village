import json
from datetime import datetime
from django.shortcuts import render
from django.views.generic import FormView, TemplateView, ListView, DetailView
from django.http import HttpResponse
from art_profile.forms import Art_imageForm, UploaddetailForm
from django.core import serializers
from django.template.loader import render_to_string
# Create your views here.
from art_profile.models import Category, SubCategory, Login_info_table
import base64
from django.core.files.base import ContentFile
from django.template.defaultfilters import slugify

class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        return context


class SuccessView(TemplateView):
    form_class = Art_imageForm
    template_name = 'sell.html'

    def dispatch(self, *args, **kwargs):
        return super(SuccessView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SuccessView, self).get_context_data(**kwargs)
        context['form'] = self.form_class()
        context['category'] = Category.objects.all()
        return context

    #
    def post(self, request, *args, **kwargs):
        user = request.user
        response_data = {}
        form = self.form_class(request.POST, request.FILES)
        level = request.POST.get('type')
        if level == "level1":
            image_file = request.POST.get('firstimage')
            request.session['image'] = image_file
        elif level == "level2":
            request.session['image'] = request.POST.get('secondimage')
            request.session['content_adult_type'] = False
            if request.POST.get('content_adult_type') == 'on':
                request.session['content_adult_type'] = True

            request.session['copyright'] = False
            if request.POST.get('copyright') == 'on':
                request.session['copyright'] = True

            request.session['title'] = request.POST.get('title')
            request.session['description'] = request.POST.get('description')
            request.session['tags'] = request.POST.getlist('tags')
            request.session['category'] = request.POST.getlist('category')
            request.session['status'] = "Draft"
        if request.POST.get('method_type') == 'get_tags':
            category = request.POST.get('id')
            thepost = Category.objects.get(id=category)
            resultset = thepost.tag_category.all()
            response_data['demo_data'] = render_to_string('ajax/list_tags.html',
                                                          {'tags': resultset})
        if request.POST.get('method_type') == 'save_design':

            data = request.session.get('image')
            if data:
                format, imgstr = data.split(';base64,')
                ext = format.split('/')[-1]
                date_str = datetime.utcnow()
                data = ContentFile(base64.b64decode(imgstr),
                                   name=str('art_image') + date_str.strftime(
                                       "%Y_%m_%d_%H_%M_%S_%f") + '.' + ext)
                category = Category.objects.get(id=request.session.get('category')[0])
                login_table = Login_info_table.objects.create(user=request.user,content_adult_type=request.session.get('content_adult_type'),copyright=request.session.get('copyright'), title=request.session.get('title'),
                                                image_filename=data, description=request.session.get('description'), status="Publish")
                login_table.category = category
                for tag in request.session.get('tags'):
                    try:
                        sub_category = SubCategory.objects.get(id=tag)
                    except:
                        sub_category =SubCategory.objects.create(tag=tag,category=category)
                    login_table.tags.add(sub_category)
                login_table.save()
                request.session['status'] = "Publish"
                del request.session['content_adult_type']
                del request.session['copyright']
                del request.session['title']
                del request.session['image']
                del request.session['category']
                del request.session['tags']
                del request.session['status']



        return HttpResponse(json.dumps(response_data), content_type="application/json")


class PublishView(TemplateView):
    template_name = 'products.html'

    def get_context_data(self, **kwargs):
        context = super(PublishView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        return context


class AllproductView(ListView):
    model = Login_info_table
    template_name = 'collection.html'

    def get_context_data(self, **kwargs):
        context = super(AllproductView, self).get_context_data(**kwargs)
        # print "Sssssssssss"
        # for table in Login_info_table.objects.all():
        #     table.save()
        #     print  table.slug
        context['category'] = Category.objects.all()
        context['productlist'] = Login_info_table.objects.filter(user=self.request.user)
        return context

    # def update_profile(request, user):
    #     slugs = Login_info_table.objects.get(user=request.user)
    #     slugs.slug = '12341234'
    #     for slug in Login_info_table.objects.all():
    #     user.profile.bio = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit...'
    #     user.save()
    # def create_slug(apps):
    #     Login_info_table = apps.get_model("slug", "Login_info_table")
    #     for slugs in Login_info_table.objects.all():
    #         slugs.slug = slugify(slugs.name)
    #         slugs.save()
    # def create(self):
    #     action = Login_info_table(slug="1234")
    #     action.save()

class SingleproductView(DetailView):
    model = Login_info_table
    form_class = UploaddetailForm
    template_name = 'single-product.html'
    slug_url_kwarg = 'productlist_slug'

    def get_context_data(self, **kwargs):
        context = super(SingleproductView, self).get_context_data(**kwargs)
        context['form'] = self.form_class()
        context['category'] = Category.objects.all()
        context['singleproduct'] = Login_info_table.objects.get(slug=self.kwargs.get('productlist_slug'))
        return context

    def post(self, request, *args, **kwargs):
        user = request.user
        response_data = {}
        id = request.POST.get('valueid')
        print id
        profile = Login_info_table.objects.get(id=id)
        form = self.form_class(request.POST, request.FILES, instance=profile)
        image_filename = request.POST.get('image_filename')
        if form.is_valid():
            form.save(user)
            response_data['status'] = True
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()

        return HttpResponse(json.dumps(response_data), content_type="application/json")


