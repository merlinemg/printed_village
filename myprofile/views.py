from django.shortcuts import render
import json
from datetime import datetime

from django.views.generic import FormView, TemplateView, ListView, DetailView
from django.http import HttpResponse
from art_profile.forms import Art_imageForm, UploaddetailForm
from django.core import serializers
from django.template.loader import render_to_string
# Create your views here.
from art_profile.models import Category, SubCategory, Login_info_table, Collection
import base64
from django.core.files.base import ContentFile
from django.template.defaultfilters import slugify
# Create your views here.
from django.shortcuts import render
from myprofile.forms import My_collectionForm


class MycollectionView(TemplateView):
    form_class = My_collectionForm
    template_name = 'my-collection.html'

    def get_context_data(self, **kwargs):
        context = super(MycollectionView, self).get_context_data(**kwargs)
        context['form'] = self.form_class()
        context['category'] = Category.objects.all()

        # context['collection_all'] = Login_info_table.objects.get(user=self.request.user).collection_products.all()
        context['collect'] = Collection.objects.filter(user=self.request.user)
        collection_value = []
        for collect in Collection.objects.filter(user=self.request.user):

                collection_value.append({'all_collection': collect, 'product': collect.products.all()[:3]})

        context['collection_value'] = collection_value
        return context

    def post(self, request, *args, **kwargs):
        print "enter"
        user = request.user
        response_data = {}
        form = self.form_class(request.POST)
        if form.is_valid():
            print "hii"
            form.save(user)
            response_data['status'] = True
        else:
            response_data['status'] = False
            response_data['errors'] = form.errors
            print form.errors.as_text()
        return HttpResponse(json.dumps(response_data), content_type="application/json")

class ProductcategroizedView(TemplateView):
    template_name = 'single-category.html'

    def get_context_data(self, **kwargs):
        context = super(ProductcategroizedView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        # context['collection_all'] = Login_info_table.objects.get(user=self.request.user).collection_products.all()
        context['categorized_product'] = Login_info_table.objects.filter(category=self.kwargs.get('catgery_id'))

        return context



class AllcollectionView(TemplateView):
    template_name = 'all_collection.html'

    def get_context_data(self, **kwargs):
        context = super(AllcollectionView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        # context['collection_all'] = Login_info_table.objects.get(user=self.request.user).collection_products.all()
        context['all_collection'] = Collection.objects.all()
        collection_value = []
        for collect in Collection.objects.all():
            if collect.products.all():
                collection_value.append({'collection': collect, 'product': collect.products.all()[:3]})


        context['collection_value'] = collection_value
        return context


class ShareartView(TemplateView):
    template_name = 'share_art_static.html'

    def get_context_data(self, **kwargs):
        context = super(ShareartView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        return context

class CollectionlistView(TemplateView):
    template_name = 'list_collection_products.html'

    def get_context_data(self, **kwargs):
        context = super(CollectionlistView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        context['list_collection'] = Collection.objects.filter(id=self.kwargs.get('my_id'))
        return context


class ProductView(DetailView):
    model = Login_info_table

    template_name = 'single-category-product.html'
    slug_url_kwarg = 'categorized_slug'

    def get_context_data(self, **kwargs):
        context = super(ProductView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        context['product'] = Login_info_table.objects.get(slug=self.kwargs.get('categorized_slug'))
        return context

    def post(self, request, *args, **kwargs):

        response_data = {}

        method_type = request.POST.get('method_type')
        if method_type == "addcollection":

            addcollection = Collection.objects.get(id=request.POST.get('collection_id'))
            product = Login_info_table.objects.get(id=request.POST.get('product_id'))
            if not addcollection.products.filter(id=product.id):
                addcollection.products.add(product)

        return HttpResponse(json.dumps(response_data), content_type="application/json")


class LikeView(TemplateView):
    template_name =  'single-category-product.html'

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            print "posttttttttt"
            response_data = {}
            user = request.user
            product = Login_info_table.objects.filter(id=self.request.POST.get('product_id'))
            print product


            return HttpResponse(json.dumps(response_data), content_type="application/json")