from __future__ import unicode_literals

from django.db import models

# Create your models here.
from art_profile.models import Login_info_table
from printed_vilage import settings


class Likestatus(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='likeuser_detail', null=True, blank=True)
    products = models.ForeignKey(Login_info_table, related_name='like_products', null=True, blank=True)
    like_status = models.BooleanField(default=False)

    def __str__(self):  # __unicode__ on Python 2
        return self.like_status