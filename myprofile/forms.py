from django import forms
from django.forms import PasswordInput, TextInput,FileInput
from django.contrib.auth import authenticate, login

from art_profile.models import Collection

class My_collectionForm(forms.ModelForm):
    collection_name = forms.CharField(required=True)
    class Meta:
        model = Collection
        fields = ['collection_name', 'collection_description']

    def save(self, user, commit=True):
        form = super(My_collectionForm, self).save(commit=False)
        if commit:
            form.user = user
            form.save()
        return form




