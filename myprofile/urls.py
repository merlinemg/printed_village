from django.conf.urls import url
from myprofile.views import MycollectionView, ProductcategroizedView, AllcollectionView, ShareartView, ProductView, CollectionlistView, LikeView

urlpatterns = [

    url(r'^mycollection/', MycollectionView.as_view(), name='mycollection'),
    url(r'^cat_list/(?P<catgery_id>[\w-]+)/', ProductcategroizedView.as_view(), name='product_catlist'),
    url(r'^allcollection/', AllcollectionView.as_view(), name='allcollection'),
    url(r'^share_art/', ShareartView.as_view(), name='shareart'),
    url(r'^list/(?P<my_id>[0-9]+)/', CollectionlistView.as_view(), name='collectionlisting'),
    url(r'^(?P<categorized_slug>[\w-]+)/', ProductView.as_view(), name='product'),
    url(r'^like/', LikeView.as_view(), name='like'),


]