
//Loader
$(window).load(function() {
	// Animate loader off screen
	$(".se-pre-con").fadeOut("slow");
});


// back to top

$(window).scroll(function() {
	if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
		$('#return-to-top').fadeIn(200);    // Fade in the arrow
	} else {
		$('#return-to-top').fadeOut(200);   // Else fade out the arrow
	}
});
$('#return-to-top').click(function(e) {
	e.preventDefault();     									// When arrow is clicked
	$('body').animate({
		scrollTop : 0                       // Scroll to top of body
	}, 500);
});


//slick slider

$(document).on('ready', function() {
	$(".regular").slick({
		dots: false,
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 3,
		autoplay: true,

		responsive:
		[
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 2,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
});

// Ripple

$(function(){
	var ink, d, x, y;
	$(".ripplelink").click(function(e){
		if($(this).find(".ink").length === 0){
			$(this).prepend("<span class='ink'></span>");
		}

		ink = $(this).find(".ink");
		ink.removeClass("animate");

		if(!ink.height() && !ink.width()){
			d = Math.max($(this).outerWidth(), $(this).outerHeight());
			ink.css({height: d, width: d});
		}

		x = e.pageX - $(this).offset().left - ink.width()/2;
		y = e.pageY - $(this).offset().top - ink.height()/2;

		ink.css({top: y+'px', left: x+'px'}).addClass("animate");
	});
});


// Nice Scroll

$(document).ready(function() {
	$("html").niceScroll({cursorwidth: '10px', railwidth: '10px', cursorcolor: '#000', background:'#000', cursorborderradius: '0px',cursorborder: '0', background:'#fff', autohidemode: true, zindex: 99999999999999 });
});

// smooth

jQuery(document).ready(function($) {
	$(".scroll").click(function(event) {
		event.preventDefault();
		$('html,body').animate( { scrollTop:$(this.hash).offset().top } , 1000);
	});
});
